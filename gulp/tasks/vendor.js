var gulp = require('gulp');
var config = require('../config');

gulp.task('vendor', function() {
  return gulp.src(config.vendor)
    .pipe(gulp.dest(config.dest));
});