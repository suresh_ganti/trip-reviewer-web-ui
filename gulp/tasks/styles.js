var gulp = require('gulp');
var config = require('../config');

gulp.task('styles', function() {
  return gulp.src(config.styles)
    .pipe(gulp.dest(config.dest));
});
