var browserSync = require('browser-sync');
var gulp = require('gulp');
var config = require('../config').browserSync;

var proxyMiddleware = require('http-proxy-middleware');
var proxyURL = 'http://tr-engine-dev.elasticbeanstalk.com';
var tripReviewURL = 'http://tripreviewerloadbalancer-738767480.us-west-2.elb.amazonaws.com';

gulp.task('browserSync', ['webpack', 'markup', 'images', 'mocks'], function() {
  //var proxy = proxyMiddleware('/services', {target: proxyURL});
  config.server.middleware = [
    proxyMiddleware('/hotel', {target: proxyURL}),
    proxyMiddleware('/services', {target: tripReviewURL})
  ];

  browserSync(config);
});
