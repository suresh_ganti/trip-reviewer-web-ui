const baseURL = 'http://tr-engine-dev.elasticbeanstalk.com';
//const legacyURL = 'http://tripreviewerloadbalancer-738767480.us-west-2.elb.amazonaws.com/services';
const legacyURL = "http://www.tripreviewer.com/restaurants/services";
//const baseURL = 'http://trengine-env.us-west-2.elasticbeanstalk.com';
//const legacyURL = 'http://dfc2flrtroer5.cloudfront.net/restaurants/services';

let mocks = ()=> {
  return location.href.indexOf('mocks') !== -1;
};

let isAbsolute = ()=> {
  return location.href.indexOf('absolute') !== -1;
};

let server = ()=> {
  return location.host.indexOf('localhost') === -1;
};

let getURL = (url, mockURL, absolute = true)=> {
  absolute = absolute || isAbsolute();
  if (mockURL && mocks()) {
    return mockURL;
  } else if (server()) {
    return absolute ? baseURL + url : url;
  } else {
	  return absolute ? baseURL + url : url;
    //return url;
  }
};

let getTripReviewerURL = (url, absolute = true) => {
  absolute = absolute || isAbsolute();
  if (server()) {
    return absolute ? legacyURL + url : '/services' + url;
  } else {
	  return absolute ? legacyURL + url : '/services' + url;
	  //return '/services' + url;
  }
};

export default {
  mocks: mocks,
  server: server,
  getTripReviewerURL: getTripReviewerURL,
  getServiceURL: getURL
};