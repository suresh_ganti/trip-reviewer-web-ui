export function extractHotels() {
  let a = $('.pophotel figure');
  let params = [];
  each(a, (f)=> {
    var $el = $(f);
    var imgURL = $el.find('img').attr('src');
    var city = $el.find('h6').text();
    var state = $el.find('p').text();
    let param = {};

    param['id'] = $el.attr('id');
    param['destType'] = $el.data('destType');
    param['destValue'] = $el.data('destvalue');
    param['imageURL'] = imgURL;
    param['city'] = city;
    param['state'] = state;
    params.push(param);
  });
}