let isDebug = () => {
  return location.href.indexOf('debug') !== -1;
};
let getConsole = ()=> {
  if (window.console) {
    return window.console;
  }
};

let log = (msg, data)=> {
  let logger = getConsole();
  if (logger && isDebug()) {
    logger.time(msg);
    logger.log(msg, data);
  }
};

let start = (key)=> {
  let logger = getConsole();
  if (logger && isDebug()) {
    logger.log("START:" + key);
    logger.time(key);
  }
};
let end = (key)=> {
  let logger = getConsole();
  if (logger && isDebug()) {
    logger.timeEnd(key);
    logger.log("END:" + key);
  }
};

export default {
  log: log,
  start: start,
  end: end
};