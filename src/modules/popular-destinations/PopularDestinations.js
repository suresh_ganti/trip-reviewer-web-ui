import HotelDestinations from './HotelDestinations';

export default angular.module('PopularDestinations', [])
  .controller('HotelDestinations', HotelDestinations);
