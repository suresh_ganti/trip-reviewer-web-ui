import popularHotels from './popular-hotels.js';
import {each} from 'lodash/collection';
import {clone} from 'lodash/lang';

class HotelDestinations {

  constructor($scope, StateUtils) {
    $scope.popularHotels = popularHotels;
    let today = moment().format('l');
    let tomorrow = moment().add(1, 'days').format('l');
    $scope.showHotels = (hotel) => {
      hotel.in = today;
      hotel.out = tomorrow;
      hotel.trigger = "popular-hotels";
      StateUtils.showPopularHotels(hotel, clone(hotel));
    };
  }

}

HotelDestinations.$inject = ['$scope', 'StateUtils'];

export default HotelDestinations;