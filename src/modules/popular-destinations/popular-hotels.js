export default[{
  "id": "e9db4a802c2fdf9deef79ca7f7e6c09703054fc8",
  "type": "CITIES",
  "where": "New york city",
  "imageURL": "images/newyork.jpg",
  "city": "New york city",
  "state": "New york, USA",
  "title":"Where to stay in new York| Top NYC hotels",
  "metaDesc":"New York City is ranked as the most photographed city in the world. For the best experience and the best NY hotel to stay at or dine in follow Tripreviewer.",
  "keywords":"hotels near Manhattan, lower cost Manhattan hotels, famous nyc hotels, famous new York hotels, new York New York hotel, coolest hotels in new York, famous hotel in new York, best places to stay in nyc, hotel new York booking, city hotels, where to stay in new York, downtown nyc hotels, top nyc hotels, best hotel new York, best New York hotel"
}, {
  "id": "0a3f0fb0b3e37be1b6602c5bfe9968c60492198b",
  "type": "CITIES",
  "where": "Las Vegas, Nevada",
  "imageURL": "images/lasvega.jpg",
  "city": "Las Vegas",
  "state": "Nevada, USA",
  "title":"Where to stay in Las Vegas| Best hotels in Las Vegas",
  "metaDesc":"Las Vegas, entertainment capital of the world, has hundreds of hotels. To have a fun-filled, enjoyable stay at this resort city, log on to TripReviewer.com",
  "keywords":"The best Vegas hotels, best hotel in las Vegas, pet friendly hotels las Vegas, top hotels in las Vegas, 5 star hotels in las Vegas, best hotel las Vegas, best place to stay in las Vegas, luxury hotels in las Vegas, las Vegas new hotels, hotels in las Vegas downtown, las Vegas 4 star hotels, top five hotels in las Vegas"
}, {
  "id": "74ff2be3413aeec7450c7e525cb04273be8f521c",
  "type": "CITIES",
  "where": "San Francisco, California",
  "imageURL": "images/sanfransico.jpg",
  "city": "San Francisco",
  "state": "California, USA",
  "title":"Book hotels in San Francisco| Get the deals on hotels",
  "metaDesc":"Best known for its pleasant summers, San Francisco is a must-see travel destination. Tripreviewer can help you find the best hotels in San Francisco to visit.",
  "keywords":"Hotels in San Francisco, San Francisco hotels, San Francisco hotels, san Francisco hotel, cheap hotels in san Francisco, where to stay in san Francisco, san Francisco hotel deals, cheap hotels san Francisco, san Francisco hotels, hotels in san Francisco, cheap hotels in san Francisco, hotels near san Francisco, san Francisco hotels downtown, cheap san Francisco hotels, luxury hotels san Francisco, cheap hotels near san Francisco, best budget hotels in san Francisco, best hotels san Francisco, top 10 hotels san Francisco, hotels close to sfo international airport"
}, {
  "id": "1550d1c24472bef2066c691be31cb9a8f629bb35",
  "type": "CITIES",
  "where": "Chicago, Illinois",
  "imageURL": "images/chicago.jpg",
  "city": "Chicago",
  "state": "Illinois, USA",
  "title":"Reviews of all hotels in Chicago| Best hotel in Chicago",
  "metaDesc":"Tripreviewer enables you to find expert advice. Book the right hotel, enjoy your stay in Chicago whether it�s a budget-friendly or luxury family-friendly hotel ",
  "keywords":"hotel Chicago downtown, top hotels in Chicago, bed and breakfast Chicago, hotels near Chicago, best Chicago hotels, hotels Chicago il, nice hotels in Chicago, best hotels in Chicago, hotel Chicago, Chicago o hare hotels, new hotels in Chicago, Chicago hotels cheap, Chicago bed and breakfast, Marriott hotels in Chicago, The Drake Hotel, medium hotels in Chicago, Trump International Hotel and Tower, Budget  friendly hotels in Chicago, hotels in the loop Chicago, hotels south Chicago, park Hyatt hotel Chicago"
}, {
  "id": "9da89f886f458f1d511684fea53640aac2a9119b",
  "type": "CITIES", "where": "Orlando, Florida",
  "imageURL": "images/orlando.jpg",
  "city": "Orlando",
  "state": "Florida, USA",
  "title":"Top hotels in Orlando| best place to stay in Orlando",
  "metaDesc":"Orlando, the theme park capital of the world, attracts millions of visitors. Tripreviewer provides best accommodation from luxury hotels to budget hotels.",
  "keywords":"Gaylord hotel Orlando, castle hotel Orlando, swan hotel Orlando, dolphin hotel Orlando, nickelodeon hotel Orlando, Disney resorts Orlando, Portofino hotel Orlando, Rosen hotel Orlando, Peabody hotel Orlando, universal Orlando hotels, hotels in Orlando international drive, hotels downtown Orlando, Victoria and Albert Disney, Victoria and Albert�s price, Disney�s spirit of aloha dinner, captain�s grille, top hotels in Orlando, best places to stay in Orlando, 5 star hotels Orlando, universal Orlando hotel"
}, {
  "id": "7456e5179b73b813ddd4da494f2aa9de87958b5a",
  "type": "CITIES",
  "where": "Los Angeles, Califonia",
  "imageURL": "images/losangles.jpg",
  "city": "Los Angeles",
  "state": "Califonia, USA",
  "title":"Where to stay in Los Angels| Best hotels in Los Angels",
  "metaDesc":"Whether you�re looking for the luxury hotel or a budget hotel at Los Angeles, Tripreviewer has all the information you need to book and enjoy your stay at LA.",
  "keywords":"Los Angeles hotel reviews, best area to stay in los Angeles, best location to stay in los Angeles, luxury hotels near losAngeles, losAngeles hotel room, cool los Angeles hotels, hotels near downtown la, best boutique hotels los Angeles, Venice beach hotel , Venice beach suites and hotel, ocean view hotel Santa Monica, hotels in downtown la, luxury hotel in Beverly hills, los Angeles Disneyland, losAngeles Hollywood, holiday inn express south, four seasons hotel los Angeles at Beverly hills, airport hotel la, where to stay in losAngeles, losAngeles international airport hotel"
}, {
  "id": "bab3f097b049628e8486ff9b4953b671e1588f47",
  "type": "CITIES",
  "where": "Miami,Florida",
  "imageURL": "images/miami.jpg",
  "city": "Miami",
  "state": "Florida, USA",
  "title":"Looking for hotels in Miami| Book Hotels in online",
  "metaDesc":"Ranked as one of the richest and cleanest cities in the US. Tripreviewer help you to find and book a luxury family hotel to make your stay truly worthwhile. ",
  "keywords":"best hotel in Miami, best Miami hotel, the hotel of south beach, best hotels in Miami, w hotel Miami, Miami south beach, south beach Miami ,Miami best hotel, chesterfield hotel Miami, Shelburne hotel Miami, downtown Miami, nightlife Miami, Miami international airport hotel, Miami downtown hotels, Miami international hotel, best hotels in Miami,chesterfield hotel Miami, executive hotel Miami, executive airport hotel Miamifl , best Miami hotel, the hotel of south beach, sagamore hotel Miami,national hotel Miami, holiday inn Miami, nightlife Miami hotels"
}, {
  "id": "b8b67ca78ee0d3eb1d03a0c4b1e2d077723e12dd",
  "type": "CITIES",
  "where": "Boston, Massachusetts",
  "imageURL": "images/boston.jpg",
  "city": "Boston",
  "state": "Massachusetts, USA",
  "title":"Boston hotel Review| Best hotel to stay in Boston",
  "metaDesc":"Boston has thousands of hotels to choose. Tripreviewer helps you compare prices and search for the best Boston City hotels, read the reviews and book online.",
  "keywords":"cheap hotels in Boston, hotels near Boston airport, Boston hotels cheap, hotels in back bay Boston, cheap hotels near Fenway park, hotels close to Boston airport, hotels near Boston Logan international airport, hotels near beacon hill Boston, cheap hotels near Boston university, hotels near Kenmore square Boston, hotels near Boston downtown, hotels in back bay Copley square, closest hotel to Boston university, top ranked discount hotels in Boston, hotels near Charlestown Boston, hotels near Fenway park, downtown Boston hotels, best hotels in Boston, cheap hotels Boston, Boston Logan airport hotels, hotels at Logan airport, Boston hotels downtown, Logan airport hotel, Boston cheap hotels, hotels in downtown Boston, Fenway park hotels, cheap hotels in Boston area"
}, {
  "id": "274e271d76609ab594763da7657bd45bf033cea0",
  "type": "CITIES",
  "where": "Hawaii",
  "imageURL": "images/hawai.jpg",
  "city": "Hawaii",
  "state": "Hawaii, USA",
  "title":"Hawaii hotel reviews| Best Hotel to stay in Hawaii",
  "metaDesc":"Read the Hawaii hotel reviews on Tripreviewer to enjoy an underwater adventure or a leisurely holiday on Hawaii�s beautiful beaches.",
  "keywords":"where to stay in Hawaii, Hawaii luxury resort, best resorts Hawaii, best place to stay in Hawaii, top Hawaii hotels, best places to stay in Hawaii, luxury hotel Hawaii ,beautiful resorts in Hawaii, best hotel Oahu, Waikiki beach resorts, luxury resorts Maui, best Hawaii hotel, Hawaii top resorts, Hawaii hotel reviews, best places to stay in Hawaii on a budget, luxury hotel in Hawaii, best place to stay in Hawaii for couples, best hotels big island Hawaii, best hotels in Hawaii for families, hotels on the beach in Hawaii, cheapest places to stay in Hawaii, Hawaii accommodation 5 star, best accommodation in Hawaii."
}, {
  "id": "30109a443164a266bd67b01b355952c25eb43a04",
  "type": "CITIES",
  "where": "Washington DC",
  "imageURL": "images/washington.jpg",
  "city": "Washington",
  "state": "DC, USA",
  "title":"Best place to stay in Washington| Washington hotel reviews",
  "metaDesc":"Planning a sight-seeing tour to Washington DC? Tripreviewer can help you discover its many attractions and find budget-friendly hotels.",
  "keywords":"Top Washington hotels, famous dc hotels, best places to stay near, Washington dc, coolest hotels in dc, hotels near Washington metro, best budget hotels Washington dc, district hotel Washington dc reviews, Americana hotel Washington dc, best place to stay in Washington dc, best hotels near Washington dc, best family hotels Washington dc, best luxury hotels in dc, Washington hotel Washington dc, hotels near Washington, best hotels to stay in Washington dc, family friendly hotels in Washington dc, Washington dc hotels with indoor pools, historic hotels Washington dc, 5 star hotels in Washington dc, top 10 hotels in Washington dc, hotel in downtown Washington dc, cheap places to stay in Washington dc, hotels in foggy bottom Washington dc, hotels in Georgetown dc, Georgetown Washington dc hotels"
}, {
  "id": "0246a33704df99cf9993eeb533758b24d2dbd526",
  "type": "CITIES",
  "where": "New Orleans, LA",
  "imageURL": "images/neworleans.jpg",
  "city": "New Orleans",
  "state": "LA, USA",
  "title":"New Orleans hotel reviews| Best New Orleans hotel",
  "metaDesc":"Discover the best time to visit the city of New Orleans, most famous for its annual Mardi Gras festival. Read the New Orleans hotel reviews on Tripreviewer.",
  "keywords":"new Orleans w hotel, the w hotel new Orleans, coolest hotels in new Orleans, w hotel new Orleans French quarter, garden district hotel new Orleans, Loews hotel new Orleans, bourbon Orleans hotel new Orleans la, best new Orleans hotel, bourbon hotel new Orleans, luxury hotel new Orleans, new Orleans hotel reviews, best hotels near bourbon street, hotels on canal street in new Orleans, garden district hotels new Orleans, hotels in new Orleans on canal street, new Orleans garden district hotels, new Orleans hotel ratings, hotels in the French quarter Louisiana, hotels in Louisiana new Orleans, new Orleans hotels garden district, new Orleans airport hotels, hotels in new Orleans near the French quarter"
}, {
  "id": "cd36ad650089f182ea21ec5d64a99add7926e185",
  "type": "CITIES",
  "where": "Seattle, WA",
  "imageURL": "images/seattle.jpg",
  "city": "Seattle",
  "state": "WA, USA",
  "title":"Best Places to stay in Seattle| Hotel Seattle reviews",
  "metaDesc":"Seattle is an attractive tourist destination. With Tripreviewer, you can find great deals and discounts on hotel bookings online.",
  "keywords":"w hotel Seattle, Seattle wa hotels, Seattle boutique hotels, best hotels Seattle, executive hotel Seattle, hotels near Seattle wa, cool hotels in Seattle, Seattle boutique hotel , Seattle hotels near space needle, hotel Seattle , best place to stay in Seattle, waterfront hotel Seattle, hotel Seattle reviews, romantic getaways near Seattle, Seattle convention centre hotels, best Seattle hotel, Seattle w hotel,  Seattle luxury hotel, five hotel Seattle,  best boutique hotels Seattle, west Seattle hotel, boutique Seattle hotels, Seattle ski resorts, ace hotel, where to stay in Seattle, inn at queen Anne , Geraldine�s counter , the ace hotel, best place to stay in Seattle, ace hotel and suites, Seattle luxury hotels , bed and breakfast Seattle, best hotels Seattle, Seattle vacation, hotel 5 Seattle."
}];