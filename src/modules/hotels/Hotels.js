import HotelSearchController from './HotelSearchController';
import HotelListController from './HotelListController';
import PhotoController from './PhotoController';
import MapController from './MapController';
import IcePortalPhotosController from './IcePortalPhotosController'

let deps = [
];

export default angular.module('Hotels', [])
  .controller('HotelListController', HotelListController)
  .controller('PhotoController', PhotoController)
  .controller('MapController', MapController)
  .controller('HotelSearchController', HotelSearchController)
  .controller('IcePortalPhotosController', IcePortalPhotosController);
