class MapController {

  constructor($scope, mapInfo, $uibModalInstance, TRService) {
    this.$scope = $scope;
    this.$scope.mapURL = TRService.getMapURL(mapInfo);

    $scope.closeMap = function() {
      $uibModalInstance.close();
    };
  }
}

MapController.$inject = ['$scope', 'mapInfo', '$uibModalInstance', 'TRService'];

export default MapController;