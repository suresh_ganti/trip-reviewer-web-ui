import TRService from './TRService';
import StateUtils from './StateUtils';
import TitleService from './TitleService';
import MetaService from './MetaService';

angular.module('TRServices', [])
  .service('StateUtils', StateUtils)
  .service('TRService', TRService)
  .service('TitleService', TitleService)
  .service('MetaService', MetaService);
