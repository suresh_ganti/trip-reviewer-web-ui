import './app';
import {contains, each} from 'lodash/collection'

let configureRoutes = (app)=> {
  app.run(['$state', '$timeout', '$rootScope', function($state, $timeout, $rootScope) {
    $timeout(()=> {
      if (contains(location.hash, '#!/home')) {
        $state.go('home', null, {reload: true});
      }
      if (contains(location.hash, '#!/')) {
        $rootScope.bodyClass = 'home';
      }
    });
  }]);

  app.config(['$sceDelegateProvider', function($sceDelegateProvider, apiUrl) {
    $sceDelegateProvider.resourceUrlWhitelist([
      'self',
      "https://www.tripadvisor.com/**",
      "http://www.tripadvisor.com/**",
      "http://www.expedia.com/**",
      "http://media.expedia.com/**",
      "https://www.expedia.com/**",
      "https://www.google.com/**",
      "https://maps.google.com/**",
      "https://www.iceportal.com/**"
    ])
  }]);
  
  app.config(['$locationProvider', function($locationProvider) {
	  $locationProvider.hashPrefix('!');
  }]);
};

let configureHttpHandlers = (app)=> {
  app.run(['$rootScope', '$http', ($rootScope, $http)=> {
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      //To cancel pending request when change state
      each($http.pendingRequests, function(request) {
        if (request.cancel && request.timeout) {
          request.cancel.resolve();
        }
      });
    });
  }]);
};

let setUISelectFilters = (app)=> {
  app.filter('propsFilter', function() {
    return function(items, props) {
      var out = [];

      if (angular.isArray(items)) {
        items.forEach(function(item) {
          var itemMatches = false;

          var keys = Object.keys(props);
          for (var i = 0; i < keys.length; i++) {
            var prop = keys[i];
            var text = props[prop].toLowerCase();
            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
              itemMatches = true;
              break;
            }
          }

          if (itemMatches) {
            out.push(item);
          }
        });
      } else {
        // Let the output be the input untouched
        out = items;
      }

      return out;
    };
  });
};

let startApp = ()=> {
  let app = angular.module('TRApp', ['TRCore']);

  configureRoutes(app);
  configureHttpHandlers(app);
  setUISelectFilters(app);
};

angular.element(document).ready(function() {
  startApp();
  angular.bootstrap(window.document, ['TRApp']);
});