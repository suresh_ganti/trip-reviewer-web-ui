import './styles/app.less';
import './modules/Modules';
import './modules/router/Router';
import './modules/services/TRServices';

let deps = [
  'ui.bootstrap',
  'Router',
  'TRModules',
  'TRServices',
  'angular-bugsnag'
];

angular.module('TRCore', deps)
  .controller('AppController', ['$rootScope', 'screenSize', 'TitleService', 'MetaService', function($rootScope, screenSize, TitleService, MetaService) {
    $rootScope.mobile = screenSize.is('xs, sm');
    $rootScope.TitleService = TitleService;
    $rootScope.MetaService = MetaService;
    screenSize.on('xs, sm', function(match) {
      $rootScope.mobile = match;
    });

  }])
  .directive('ngEnter', function() {
    return function(scope, element, attrs) {
      element.bind("keydown keypress", function(event) {
        if (event.which === 13) {
          scope.$apply(function() {
            scope.$eval(attrs.ngEnter);
          });
          event.preventDefault();
        }
      });
    };
  }).directive('escKey', function() {
    return function(scope, element, attrs) {
      element.bind('keydown keypress', function(event) {
        if (event.which === 27) { // 27 = esc key
          scope.$apply(function() {
            scope.$eval(attrs.escKey);
          });
          event.preventDefault();
        }
      });
    };
  }).directive('convertToNumber', function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, ngModel) {
        ngModel.$parsers.push(function(val) {
          return parseInt(val, 10);
        });
        ngModel.$formatters.push(function(val) {
          return '' + val;
        });
      }
    }
  }).filter('orderObjectBy', function() {
	  return function(items, field, reverse) {
		    var filtered = [];
		    var items1 = {};
		    angular.forEach(items, function(item,index) {
	    		if(item.rating){
			    	item['index'] = index;
			    	filtered.push(item);
	    		}
		    });
		    filtered.sort(function (a, b) {
		      return (a[field] > b[field] ? 1 : -1);
		    });
		    angular.forEach(filtered, function(item){
		    	items1[item['index']] = item;
		    });
		    angular.forEach(items, function(item, index){
		    	items1[index] = item;
		    });
		    if(reverse) filtered.reverse();
		    return items1;
		  };
  }).config(['bugsnagProvider', function (bugsnagProvider) {
      bugsnagProvider
      .noConflict()
      .apiKey('e39aba65780762fd2c06ec6f56abc4d6')
      .releaseStage('prod')
      .user({
          id: 123,
          name: 'Sanket Talsania',
          email: 'sankettalsania@tripreviewer.com'

      })
   }]).factory('ExceptionHandler',['$q', '$location','bugsnag', function($q, $location, bugsnag){
		return {
			requestError : function(request){
				bugsnag.notifyException(request,"Error");
			},
			responseError: function(response) {
				bugsnag.notifyException(response.data,"Error");
				return $q.reject(response);
			}
		};
	}]).config(['$httpProvider', function($HttpProvider){
		//$HttpProvider.interceptors.push('Logging');
		//$HttpProvider.interceptors.push('SlowRequest');
		$HttpProvider.interceptors.push('ExceptionHandler');
	}]);