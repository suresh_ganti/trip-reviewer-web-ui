# Hotels project third party dependencies

Follow the same process as mentioned in main project's README to install node and global node modules.

### Download dependencies

```sh
$ npm install
$ bower install
```

### Build For debug/dev (unminified)

```sh
$ gulp build

```

### Build For Production (minified)

```sh
$ gulp build --ENV=prod

```